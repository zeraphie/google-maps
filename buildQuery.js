/**
 * Build the parameters to be added onto the end of a query string from
 * an object using it's keys and values
 *
 * @param params
 * @returns {string}
 */
export default function buildQuery(params){
    return Object.keys(params).map(
        k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])
    ).join('&');
}
