# Maps Package
This is a package that helps to create, render and interact with google maps for a project

## Usage
Below is an example on how to use this package to render every map that's on the page

```javascript
import GoogleMap from './maps/GoogleMap.js';
import loadGoogleMapsApi from './maps/loadGoogleMapsApi.js';
import renderStaticMap from './maps/renderStaticMap.js';

/*==============================================================================
 Load all the maps on the page!
==============================================================================*/
let maps = $('.map');
if(!!maps.length){
    maps.each(function(){
        const mapelement = $(this);

        if(mapelement.hasClass('with-markers')) {
            const map = new GoogleMap(mapelement[0]);

            loadGoogleMapsApi(function(){
                map.renderMap().setMarkers(mapelement.data('markers'));
            });
        }

        if(mapelement.hasClass('no-markers')){
            // This utilizes the map class to make an actual google maps object
            // but for this case, a static map is actually better due to no
            // interactions with the actual map
            //
            // const map = new GoogleMap(mapelement[0]);
            //
            // loadGoogleMapsApi(function(){
            //     map.renderMap().centerMap(
            //         mapelement.data('lat'),
            //         mapelement.data('lng'),
            //         mapelement.data('zoom')
            //     );
            // });

            renderStaticMap(mapelement);
        }
    });
}
```
