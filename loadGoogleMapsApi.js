let loaded = null;

/**
 * Load the google api once only and then render each map with a callback
 *
 * @param callback
 */
export default function loadGoogleMapsApi(callback){
    // Use the Promise API to make sure that the setup map function is only
    // called once the google api is loaded, even if there's multiple maps
    if(!loaded){
        let deferred = $.Deferred();

        $.ajax({
            url: 'http://www.google.com/jsapi/',
            dataType: "script",
            success(){
                google.load('maps', '3', {
                    other_params: 'key=' + GOOGLE_API_KEY,
                    callback(){
                        deferred.resolve();
                    },
                });
            }
        });

        loaded = deferred.promise();
    }

    // Only call the render map function after the google api is loaded so that
    // the google variable is accessible
    return loaded.done(() => {
        if(typeof callback === 'function'){
            callback();
        }
    });
}
